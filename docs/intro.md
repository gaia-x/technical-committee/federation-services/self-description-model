# Gaia-X Self-Description Information Model

The purpose of this document is to technically describe the model and format of what is referred in the Gaia-X context as a "Self-Description".

## Gaia-X Specification Context

This document extends the information that the following documents provide about Self-Descriptions:

- the [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/), which 
    - defines the Conceptual Model of entities that can have Self-Descriptions (*ServiceOffering*s, their *Provider*s, and the *Resource*s they aggregate), and
    - introduces Self-Descriptions and their lifecycle on a high level, and
- the [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/), which defines
    - the schema against which claims in Self-Descriptions shall be validated, e.g., whether mandatory attributes are present and whether attribute values have the correct datatypes, as well as
    - the trust anchors to which the identity of issuers of signatures must be traceable.

## Introduction

Gaia-X Self-Descriptions are tamper-proof documents that contain the description of Gaia-X entities such as *Participant* (including *Provider*), *Service Offering*, compositions of services, and the aggregation of *Resource*s by *Service Offering*s.

## Self-Description Format and Structure

The serialization format of Verifiable Presentations, Verifiable Credentials and Credentials is JSON-LD and defined as follows.

Gaia-X Self-Description documents are **Verifiable Presentations** following the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/).  
One such Verifiable Presentation contains one or more **Verifiable Credentials**.  
Each Verifiable Credential contains **claims** about one or more *credential subjects*.

The Verifiable Credentials Data Model is defined as a vocabulary for the [W3C RDF Resource Description Framework](https://www.w3.org/TR/rdf11-concepts/) graph data model.  
Gaia-X uses the [JSON-LD](https://www.w3.org/TR/json-ld11/) serialization of RDF.

In the most typical situation in Gaia-X, a Provider *P* presents to another Participant, or to a Federation Service acting on behalf of an actual or potential Participant, a Verifiable Presentation of *P* itself or of a ServiceOffering provided by *P*.  In this case, all Verifiable Credentials in the Verifiable Presentation contain claims about the same subject.  

Gaia-X also allows Verifiable Presentations to contain Verifiable Credentials that contain claims about multiple different subjects.  Such a Verifiable Presentation may, e.g., have been produced by an Issuer who assessed multiple ServiceOfferings made by a Provider, or multiple Resources owned by a Provider, for conformance with some standard, in one bulk assessment.  

All objects exchanged and referenced in Gaia-X require Web-scalable identifiers, i.e., URIs.  It is recommended to use Decentralized Identifiers (DIDs).

### Example

The following listing shows an example of a Self-Description.
For brevity, the claims about the credential subjects are placeholders.

```json
{
  "@id": "http://example.org/verifiablePresentation/1",
  "@context": [ "https://www.w3.org/2018/credentials/v1" ],
  "@type": [
    "VerifiablePresentation"
  ],
  "issuer": "https://example.org/issuers/565049",
  "issuanceDate": "2010-01-01T00:00:00Z",
  "expirationDate": "2033-01-01T00:00:00Z",
  "verifiableCredential": [ 
  {
    "@id": "http://example.org/verifiableCred/1",
    "@type": [ "VerifiableCredential" ],
    "issuer": "https://example.org/issuers/565049",
    "issuanceDate": "2010-01-01T00:00:00Z",
    "expirationDate": "2033-01-01T00:00:00Z",
    "credentialSubject": [
        {
          "@id": "did:example:cred1.1",
          "@context": {
            "gx": "https://registry.gaia-x.eu/22.04/schema/gaia-x"
          }
          "@type": "gx:Participant",
          "gx:registrationNumber": "...",
          ...
        },
        {
          ...
        }
      ],
      "proof": { ... }
    },
    {
      ...
    }
  ],
  "proof": { ... }
}
```
