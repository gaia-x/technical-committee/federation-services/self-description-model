# Verifiable Credential and  Verifiable Presentation

## Basic Concepts

This document extends the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/) to specify how it shall be applied in the scope of Gaia-X.

### Namespace Bindings and Contexts

On the level of the Verifiable Presentation and the Verifiable Credentials contained in the Verifiable Presentation, a Self-Description MUST adhere to the vocabulary of the Verifiable Credentials Data Model, i.e., use terms from the `https://www.w3.org/2018/credentials#` namespace.

To enable human authors of Self-Descriptions to write down these terms conveniently, they MAY, by using the `@context` keyword on the level of the Verifiable Presentation, e.g.:

- reference the [JSON-LD context](https://www.w3.org/TR/json-ld11/#the-context) provided by the Verifiable Credentials Data Model (https://www.w3.org/2018/credentials/v1) like in the initial example listing, or
- define their own context, which
    - defines the above namespace as the default vocabulary using the `@vocab` keyword, or
    - maps the above namespace to a designated prefix, e.g., `"cred"`.

Similarly, the claims about any credential subject MUST adhere to the vocabulary of the Gaia-X Self-Description Schemas published in the [Gaia-X Registry](https://registry.gaia-x.eu/), or to Federation-specific specializations of this vocabulary.

### Identifiers

The `@id` MUST be present and unique for a given `issuer`.

It is up to the `issuer` to decide if the `@id` is a resolvable [URI](https://www.rfc-editor.org/rfc/rfc3986) or not.

### Types

The `@type` MUST be present in Verifiable Presentation, Verifiable Credentials, and Credentials. The only expected values for the `@type` property are 

- `"VerifiablePresentation"` for a Verifiable Presentation
- `"VerifiableCredential"` for a Verifiable Credential

The expected values for the `@type` property of a credential subject are given by the taxonomy of classes defined in the [Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/), having the superclasses `Participant`, `ServiceOffering` and `Resource`.

A Federation MAY define additional subclasses of these by further shapes hosted in its Catalogue(s).
In future, Gaia-X and federations MAY also define additional, more specific credential types.

#### Schema Validation

A Schema for Self-Descriptions, to be used as the vocabulary of the claims about credential subjects, MUST be available in the form of SHACL shapes (cf. the [W3C Shapes Constraint Language SHACL](https://www.w3.org/TR/shacl/)) in the Gaia-X Registry or in the Catalogue of a Federation.  
At any point where Self-Descriptions are created or received, a certain set of SHACL shapes is known, which forms a *shapes graph*.
A Self-Description forms a *data graph*.  For compliance with Gaia-X and/or a specific Federation, this *data graph* MUST be validated against the given *shapes graph* [according to the SHACL specification](https://www.w3.org/TR/shacl/#validation-definition).

### Issuers

The `issuer` property MUST be present in Verifiable Credential and Verifiable Presentation. The value of the `issuer` property must be a resolvable URI.  

The supported schemes for `issuer`'s URI are:

- `https`
- `did`. The supported [DID methods](https://w3c.github.io/did-spec-registries/#did-methods) are:
    - `web`

### IssuanceData

The `issuanceDate` is MANDATORY for Verifiable Credential and Verifiable Presentation.

### ExpirationDate

The `expirationDate` is RECOMMENDED for Verifiable Credential and Verifiable Presentation.

### Verifiable Credential

If the `@type` is `"VerifiablePresentation"`, the property `verifiableCredential` MUST be defined. The value of `verifiableCredential` can be a Verifiable Credential or an array of Verifiable Credentials. A Verifiable Credential MUST have 

- an [`@id`](#identifiers),
- an [`issuer`](#issuers),
- a [`@type`](#types), and
- a ['credentialSubject](#Credential_Subject)

### Credential Subject

The `credentialSubject` can be an object or array of objects, containing claims.

The claims about one Gaia-X entity MAY be spread over multiple Credentials and their subjects.

Each credential subject MUST have an [`@id`](#identifiers).

A credential subject MAY be described *by value*, i.e., by stating one or more claims about it in place.
In this case, it MUST have a [`@type`](#types) as specified below.

Alternatively, a credential subject MAY be described *by reference*.
In this case, the `@id` MUST be resolvable to an RDF resource that has the same `@id`, a `@type`, and one or more claims.

The value of the `@type` property dictates the vocabulary available for the definition of claims about the credential subject. E.g., `LegalPerson` in the [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/) defines `registrationNumber`, `headquarterAddress.countryCode` and `legalAddress.countryCode` as the vocabulary for claims about legal persons. A valid `LegalPerson` as a credential subject would thus look as follows:

```json
{
  "id":"did:example:cred2",
  "@context": {
    "gx": "https://registry.gaia-x.eu/22.04/schema/gaia-x/participant#",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@type":"gx:LegalPerson",
  "gx:registrationNumber":{
    "@value":"DEK1234R.HRB123456",
    "@type":"xsd:string"
  },
  "gx:headquarterAddress": {
    "gx:countryCode": {
      "@value": "DE",
      "@type": "xsd:string"
    }
  },
  "gx:legalAddress": {
    "gx:countryCode": {
      "@value":"DE",
      "@type":"xsd:string"
    }
  }
}
```

### Proofs (Signatures)

If the `@type` is `"VerifiableCredential"`, then at least one `proof` MUST be included.

The supported `@type` of proof is [`JsonWebSignature2020`](https://w3c-ccg.github.io/lds-jws2020/) using `JsonWebKey2020` verification method.

Before signing, the Self-Description must be normalised using the Universal RDF Dataset Canonicalization Algorithm 2015 ([URDNA2015](https://json-ld.github.io/rdf-dataset-canonicalization/spec/)). Then, the normalised Self-Description must be hashed with `SHA256`. Lastly, the hash (`SHA256(normalisedSelfDescription)`) is signed resulting in a `jws`. The signature must use the JWS Unencoded Payload Option ([RFC7797](https://www.rfc-editor.org/rfc/rfc7797)) with detached payload.

