# Gaia-X compliance input/output

The Gaia-X Compliance service

```mermaid
flowchart LR

in[Verifiable Presentation]
compliance[Gaia-X Compliance]
out[Verifiable Credential]

in --> |input|compliance --> |output|out
```

## Input

The input of the Gaia-X Compliance service is a `Verifiable Presentation` that might contain:

- a Verifiable Credential [by value](#byvalue)
- a Verifiable Credential [by reference](#byreference)

Such a Verifiable Credential may or may not be covered by the [Gaia-X Compliance rules](http://docs.gaia-x.eu/policy-rules-committee/trust-framework/).

The following example contains fake, placeholder attributes for `Participant` and `ServiceOffering`, which are not valid against the Gaia-X Self-Description Schema.

```json
{
  "@id": "http://example.edu/verifiablePresentation/1",
  "@context": [
    "https://www.w3.org/2018/credentials/v1"
  ],
  "type": [
    "VerifiablePresentation"
  ],
  "issuer": "https://example.edu/issuers/565048",
  "issuanceDate": "2010-01-01T00:00:00Z",
  "verifiableCredential": [
    {
      "@id": "http://example.edu/VerifiableCred/1"
    },
    {
      "@id": "http://example.edu/verifiableCred/2",
      "type": [
        "VerifiableCredential"
      ],
      "issuer": "https://example.edu/issuers/565049",
      "issuanceDate": "2010-01-01T00:00:00Z",
      "credentialSubject": [
        {
          "@id": "did:example:cred1",
          "type": "Participant",
          "claim1": {
            "@value": "foo",
            "@type": "xsd:string"
          },
          "claim2": {
            "@value": "bar",
            "@type": "xsd:string"
          }
        },
        {
          "id": "did:example:cred2",
          "type": "ServiceOffering",
          "claim1": {
            "@value": "foo",
            "@type": "xsd:string"
          },
          "claim2": {
            "@value": "bar",
            "@type": "xsd:string"
          }
        }
      ],
      "proof": {
        "type": "JsonWebSignature2020",
        "created": "2022-02-25T14:58:43Z",
        "verificationMethod": "https://example.edu/issuers/565049#key-1",
        "proofPurpose": "assertionMethod",
        "jws": "z2iiwEyyGcqwLPMQDXnjEdQU4zGzWs6cgjrmXAM4LRcfXni1PpZ44EBuU6o2EnkXr4uNMVJcMbaYTLBg3WYLbev3S"
      }
    }
  ]
}
```

## Output

The output of the Gaia-X Compliance service is a `VerifiableCredential` containing the `id` and `hash` of the compliant `VerifiableCredential` from the input.


```json
{
  "id": "http://example.edu/complianceResult/1",
  "type": [
    "VerifiableCredential"
  ],
  "issuer": "https://example.edu/issuers/565050",
  "issuanceDate": "2010-01-01T00:00:00Z",
  "expirationDate": "2010-03-01T00:00:00Z",
  "credentialSubject": [
    {
      "type": "gaiaxCompliance",
      "id": "http://example.edu/verifiableCred/1",
      "hash": "0f5ced733003d11798006639a5200db78206e43c85aa0386d7909c3e6c8ed535"
    },
    {
      "type": "gaiaxCompliance",
      "id": "http://example.edu/verifiableCred/2",
      "hash": {
        "@value": "0f5ced733003d11798006639a5200db78206e43c85aa123456789789123456798",
        "@type": "xsd:string",
        "@checksumtype": "SHA-256"
      }
    }
  ],
  "proof": {
    "type": "JsonWebSignature2020",
    "created": "2022-06-12T19:38:26.853Z",
    "proofPurpose": "assertionMethod",
    "jws": "z2iiwEyyGcqwLPMQDXnjEdQU4zGzWs6cgjrmXAM4LRcfXni1PpZ44EBuU6o2EnkXr4uNMVJcMbaYTLBg3WYLbev3S",
    "verificationMethod": "did:web:compliance.gaia-x.eu"
  }
}
```
