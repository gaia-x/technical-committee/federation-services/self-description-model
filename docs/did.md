# Decentralized Identifiers

## Basic Concepts

This document extends the [W3C Decentralized Identifiers](https://www.w3.org/TR/did-core/) to specify how it shall be applied in the scope of Gaia-X.

### Verification Methods

To ensure a Self-Description's integrity and authenticity, its claims must be cryptographically signed by the Participant that is issuing them.  
This is done to avoid tampering and to technically allow to check the origin of the claims.

The supported verification methods are described below.

#### JSON Web Key 2020

This section extends the specification from the [W3C JSON Web Key 2020](https://w3c-ccg.github.io/lds-jws2020/#json-web-key-2020).

A Verifiable Credential is *Gaia-X conformant* if the issuer of the Verifiable Credential itself has an identity coming from one of the [Trust Anchors](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/trust_anchors/).

Without a means to link the issuer's verification method to a Gaia-X Trust Anchor, the Gaia-X Compliance verification will fail.

To be able to assess the chain of trust, the `publicKeyJwk` property should include the [RFC7517 `x5c`](https://www.rfc-editor.org/rfc/rfc7517#section-4.7) (X.509 Certificate Chain) parameter or [RFC7517 `x5u`](https://www.rfc-editor.org/rfc/rfc7517#section-4.6) (X.509 URL) parameter.  
The `x5u` parameter should be resolvable to a `X509` `.crt`, `.pem`, `.der` or `.p7b` file which contains a valid Gaia-X Trust Anchor eligible for the signed claims.

```json
{
  "..."
  "verificationMethod": [
    {
      "@context": "https://w3id.org/security/suites/jws-2020/v1",
      "id": "did:web:vc.gaia-x.eu:issuer#JsonWebKey2020-Ed25519",
      "controller": "did:web:vc.gaia-x.eu:issuer",
      "type": "JsonWebKey2020",
      "publicKeyJwk": {
        "crv": "Ed25519",
        "x": "ecBxqTm7YKx9lja3U-EzxMGLNz4xzBTm_iki3LUA-zc",
        "kty": "OKP",
        "x5u": "did:web:vc.gaia-x.eu:issuer#svc-fullchain"
      }
    }
  ],
  "service": [
    {
      "id": "did:web:vc.gaia-x.eu:issuer#svc-fullchain",
      "type": "X509Certificate",
      "serviceEndpoint": "http://vc.gaia-x.eu/issuer/fullchain.crt"
    }
  ]
}
```

## Use of Identifiers in Self-Descriptions

Each of the following must have a different identifier:

* a Verifiable Presentation
* a Verifiable Credential inside a Verifiable Presentation
* the subject of a Verifiable Credential, i.e., the Conceptual Model entity that claims are made about.

In other words, the identity of a Self-Description (a Verifiable Presentation) is different from the identity of the thing that it describes (e.g., a *ServiceOffering*).

Self-Descriptions may reference other Self-Descriptions.  Consider, for example, a *ServiceOffering* that:

* is provided by a *Provider*,
* is a composition of other *ServiceOffering*s, or
* is an aggregation of *Resources*.

On the level of the graph of information about Gaia-X entities, which is formed by these claims, these relationships are between Gaia-X entities.  That is, the subject and object of such a claim are IDs of Gaia-X entities.  Thus, when the Self-Description *S_S* (a Verifiable Presentation) of a *ServiceOffering* states that the offering is made by some *Provider* *P*, the Self-Description *S_S* does not provide attestations of any claims about *P*.  Instead, these attestations must be provided in a dedicated Self-Description *S_P* of *P*, i.e., a Self-Description containing claims about the credential subject *P*.  The link from *S_S* to *S_P* is not explicit, but can be discovered by any Participant or Federation Service (e.g., a Federated Catalogue) that receives *S_S* as well as *S_P* and thus notices that their claims both refer to the same ID of *P*.

