# The document has been integrated in https://gitlab.com/gaia-x/technical-committee/federation-services/icam/

-----------

# Self-Description Model

This document is maintained by the [Service Characteristics Working Group](https://gitlab.com/gaia-x/technical-committee/service-characteristics/).

## Output

The human-readable version that's generated automatically from these sources is available at https://gaia-x.gitlab.io/technical-committee/federation-services/self-description-model/.
